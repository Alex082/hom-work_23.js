/*

1 Опишіть своїми словами, що таке обробник подій.

    кусок кода который будет работать сразу при наступлении его

2 Опишіть, як додати обробник подій до елемента. Який спосіб найкращий і чому?

    для добавления можно использовать addEventListener, и этот же метод есть самый популярный потому
    что он прине мает сразу 2 аргумента и когда мы вторым аргументом передаём функцыю которая будет 
    сделана это даст более удобный код при работе с ним

*/

// получаем инпут
const priceInput = document.getElementById("price");

// добавляем фокуч на инпут и меняем цвет
priceInput.addEventListener("focus", function () {
  priceInput.style.borderColor = "green";
});

// добавляем фокус для его потери
priceInput.addEventListener("blur", function () {
  const priceValue = priceInput.value;

  // делаем проверку число болльше или меньше 0, если меньше выводим ошибку
  if (priceValue < 0) {
    priceInput.style.borderColor = "red";
    const errorSpan = document.createElement("span");
    errorSpan.textContent = "Please enter correct price";
    errorSpan.style.color = "red";
    priceInput.parentNode.insertBefore(errorSpan, priceInput.nextSibling);
  } else if (priceValue > 0) {
    priceInput.style.borderColor = "green";
    const priceSpan = document.createElement("span");
    priceSpan.textContent = `Поточна ціна: ${priceValue}`;
    priceSpan.style.marginLeft = "10px";
    const closeButton = document.createElement("button");
    closeButton.textContent = "X";
    closeButton.style.marginLeft = "10px";

    // добавляем слик на кнопку после которого удаляем и очищаем инпут
    closeButton.addEventListener("click", function () {
      priceSpan.remove();
      priceInput.value = "";
    });

    // добавляем спан перед инпут
    priceInput.parentNode.insertBefore(priceSpan, priceInput.nextSibling);
    priceInput.parentNode.insertBefore(closeButton, priceInput.nextSibling);
  } else {
    priceInput.style.borderColor = "";
  }
});
